package luis.goyes.unittestpoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnittestpocApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnittestpocApplication.class, args);
	}

}
